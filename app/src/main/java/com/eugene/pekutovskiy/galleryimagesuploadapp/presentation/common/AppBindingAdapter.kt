package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common

import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.eugene.pekutovskiy.galleryimagesuploadapp.R

@BindingAdapter(value = ["loadImage"])
fun loadImage(view: ImageView, url: String?) {

    val options: RequestOptions = RequestOptions()
        .error(R.drawable.shape_gallery_item_placeholder)
        .placeholder(R.drawable.shape_gallery_item_placeholder)

    Glide.with(view.context)
        .load(url)
        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        .apply(options)
        .transition(DrawableTransitionOptions.withCrossFade())
        .into(view)
}

@BindingAdapter(value = ["loadWithUri"])
fun loadImage(view: ImageView, uri: Uri?) {

    val options: RequestOptions = RequestOptions()
        .error(R.drawable.shape_gallery_item_placeholder)
        .placeholder(R.drawable.shape_gallery_item_placeholder)

    Glide.with(view.context)
        .load(uri)
        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        .apply(options)
        .into(view)
}