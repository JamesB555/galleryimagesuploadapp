package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.adapter

import androidx.recyclerview.widget.DiffUtil
import javax.inject.Inject

class GalleryListDiffCallback @Inject constructor() :
    DiffUtil.ItemCallback<String>() {
    override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
        return oldItem == newItem
    }
}