package com.eugene.pekutovskiy.galleryimagesuploadapp.domain

import android.net.Uri
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo.ImagesRepository
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.base.UseCase
import io.reactivex.Single
import javax.inject.Inject

class ForceRefreshImagesUseCase @Inject constructor(
    private val repo: ImagesRepository
) : UseCase<List<Uri>> {
    override fun execute(): Single<List<Uri>> = repo.forceRefreshImages()
}