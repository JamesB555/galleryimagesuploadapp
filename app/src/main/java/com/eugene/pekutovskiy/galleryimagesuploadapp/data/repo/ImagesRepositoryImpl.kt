package com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo

import android.net.Uri
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.DataSource
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.local.LocalStore
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

class ImagesRepositoryImpl @Inject constructor(
    private val remoteDataSource: DataSource,
    private val localDataSource: LocalStore
) : ImagesRepository {

    override fun fetchImages(): Single<List<Uri>> =
        localDataSource.getImages()
            .onErrorReturnItem(Optional.empty())
            .flatMap {
                if (it.isPresent) {
                    Single.just(it.get())
                } else {
                    fetchImagesFromRemote()
                }
            }

    override fun forceRefreshImages(): Single<List<Uri>> = fetchImagesFromRemote()

    override fun uploadImage(uri: Uri): Completable =
        Completable.concatArray(
            remoteDataSource.uploadImage(uri),
            fetchImagesFromRemote()
                .ignoreElement()
                .onErrorComplete()
        )

    private fun fetchImagesFromRemote() =
        remoteDataSource
            .getImages()
            .map {
                it.sortedBy(Uri::toString)
            }
            .doOnSuccess(::cacheImages)

    private fun cacheImages(uris: List<Uri>) {
        localDataSource
            .storeUris(uris)
            .onErrorComplete()
            .subscribe()
    }
}