package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.eugene.pekutovskiy.galleryimagesuploadapp.databinding.CellGalleryItemBinding
import javax.inject.Inject

class GalleryListAdapter @Inject constructor(
    diffCallback: GalleryListDiffCallback
) : ListAdapter<String, GalleryListAdapter.ViewHolder>(diffCallback) {

    var onClickListener: ((Int) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder.create(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), onClickListener)
    }

    class ViewHolder(
        private val binding: CellGalleryItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            uri: String,
            callback: ((Int) -> Unit)?
        ) {
            binding.url = uri
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    callback?.invoke(position)
                }
            }
        }

        companion object {
            fun create(parent: ViewGroup): ViewHolder {
                val binding = CellGalleryItemBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
                val cellSize = parent.measuredWidth / 2
                binding.root.apply {
                    minimumWidth = cellSize
                    minimumHeight = cellSize
                }
                return ViewHolder(binding)
            }
        }
    }

}