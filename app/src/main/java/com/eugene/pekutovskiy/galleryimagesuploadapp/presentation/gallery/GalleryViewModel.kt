package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eugene.pekutovskiy.galleryimagesuploadapp.annotations.OpenForTesting
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.FetchImagesUseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.ForceRefreshImagesUseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.base.UseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.ConnectivityStatusProviderDelegate
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.ConnectivityStatusProviderDelegateImpl
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.rx.SchedulersFacade
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.rx.addTo
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@OpenForTesting
class GalleryViewModel @Inject constructor(
    connectivityStatusProvider: ConnectivityStatusProviderDelegateImpl,
    private val fetchImagesUseCase: FetchImagesUseCase,
    private val forceRefreshImagesUseCase: ForceRefreshImagesUseCase,
    private val schedulersFacade: SchedulersFacade
) : ViewModel(), ConnectivityStatusProviderDelegate by connectivityStatusProvider {

    val uiEventLiveData = MutableLiveData<UiEvent>()
    private val disposable = CompositeDisposable()

    fun onFabClicked() {
        uiEventLiveData.value = UiEvent.ShowChooseImageDialog
    }

    fun onSwipeRefresh() {
        fetchImages(forceRefreshImagesUseCase)
    }

    fun invalidateUiEventListener() {
        uiEventLiveData.value = null
    }

    fun populateGallery() {
        fetchImages(fetchImagesUseCase)
    }

    private fun fetchImages(useCase: UseCase<List<Uri>>) {
        useCase.execute()
            .map {
                it.map(Uri::toString)
            }
            .subscribeOn(schedulersFacade.io())
            .observeOn(schedulersFacade.main())
            .subscribe(
                {
                    uiEventLiveData.value = UiEvent.GalleryUpdate(it)
                },
                {
                    uiEventLiveData.value = UiEvent.Error(it)
                }
            ).addTo(disposable)
    }

    override fun onCleared() {
        disposable.clear()
        cleanup()
        super.onCleared()
    }

    sealed class UiEvent {
        class GalleryUpdate(val pics: List<String>) : UiEvent()
        class Error(val throwable: Throwable) : UiEvent()
        object ShowChooseImageDialog : UiEvent()
    }
}