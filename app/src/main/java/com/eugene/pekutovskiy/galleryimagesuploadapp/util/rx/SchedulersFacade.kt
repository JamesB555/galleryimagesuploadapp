package com.eugene.pekutovskiy.galleryimagesuploadapp.util.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

open class SchedulersFacade @Inject constructor() {

    open fun io(): Scheduler = Schedulers.io()

    open fun computation(): Scheduler = Schedulers.computation()

    open fun main(): Scheduler = AndroidSchedulers.mainThread()
}