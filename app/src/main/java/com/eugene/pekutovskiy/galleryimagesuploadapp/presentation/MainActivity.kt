package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.eugene.pekutovskiy.galleryimagesuploadapp.R
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.FragmentFactory
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.GALLERY_FRAGMENT
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.loadFragment
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject


class MainActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>
    @Inject
    lateinit var fragmentFactory: FragmentFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            addFragment()
        }
    }

    private fun addFragment() {
        val idFromExtras = intent.extras?.getInt(EXTRA_FRAGMENT_ID)
        val fragmentId = idFromExtras ?: GALLERY_FRAGMENT
        val fragment = fragmentFactory.createFragment(fragmentId)
        loadFragment(fragment)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector

    companion object {
        const val EXTRA_FRAGMENT_ID = "fragment_extra_id"
    }
}
