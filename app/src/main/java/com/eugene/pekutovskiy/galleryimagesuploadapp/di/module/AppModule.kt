package com.eugene.pekutovskiy.galleryimagesuploadapp.di.module

import android.content.Context
import com.eugene.pekutovskiy.galleryimagesuploadapp.SparkNetworksApp
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: SparkNetworksApp): Context {
        return application.applicationContext
    }
}