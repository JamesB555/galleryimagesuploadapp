package com.eugene.pekutovskiy.galleryimagesuploadapp.di.component

import com.eugene.pekutovskiy.galleryimagesuploadapp.SparkNetworksApp
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.DataSourceModule
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.di.FirebaseModule
import com.eugene.pekutovskiy.galleryimagesuploadapp.di.module.AppModule
import com.eugene.pekutovskiy.galleryimagesuploadapp.di.module.ContributeActivityModule
import com.eugene.pekutovskiy.galleryimagesuploadapp.di.module.ViewModelProviderModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        ContributeActivityModule::class,
        DataSourceModule::class,
        ViewModelProviderModule::class,
        FirebaseModule::class
    ]
)
interface AppComponent {

    fun inject(app: SparkNetworksApp)

    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(application: SparkNetworksApp): Builder
    }
}