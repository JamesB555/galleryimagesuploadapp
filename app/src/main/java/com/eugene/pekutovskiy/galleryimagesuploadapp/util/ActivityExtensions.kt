package com.eugene.pekutovskiy.galleryimagesuploadapp.util

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.eugene.pekutovskiy.galleryimagesuploadapp.R
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.network.exception.NoNetworkException
import com.google.android.material.snackbar.Snackbar

fun FragmentActivity.showSnackbar(
    message: String,
    onDismissEvent: (() -> Unit)? = null,
    durationFlag: Int = Snackbar.LENGTH_LONG
) {
    val contentView = this.window.decorView.findViewById<View>(android.R.id.content)
    val snackBar = Snackbar.make(contentView, message, durationFlag)
    onDismissEvent?.let {
        snackBar.addCallback(object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                it.invoke()
                snackBar.removeCallback(this)
            }
        })
    }
    snackBar.show()
}

fun FragmentActivity.setDisplayHomeAsUpEnabled(showHomeAsUp: Boolean) {
    (this as? AppCompatActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(showHomeAsUp)
}

fun FragmentActivity.loadFragment(fragment: Fragment, addToBackStack: Boolean = false) {
    val fm = requireNotNull(supportFragmentManager)
    fm.beginTransaction().apply {
        if (addToBackStack) {
            addToBackStack(null)
        }
        replace(R.id.container, fragment)
    }.commit()
}

fun FragmentActivity.showError(throwable: Throwable?, message: String? = null) {
    val errorMessage = if (throwable is NoNetworkException) {
        getString(R.string.cant_upload_while_offline)
    } else {
        message ?: throwable?.localizedMessage ?: getString(R.string.unknown_error_msg)
    }
    showSnackbar(errorMessage)
}
