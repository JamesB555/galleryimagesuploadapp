package com.eugene.pekutovskiy.galleryimagesuploadapp.domain

import android.net.Uri
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo.ImagesRepository
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.base.CompletableUseCaseWithArgs
import javax.inject.Inject

class UploadImageUseCase @Inject constructor(
    private val repo: ImagesRepository
) : CompletableUseCaseWithArgs<Uri> {

    override fun execute(arg: Uri) = repo.uploadImage(arg)
}