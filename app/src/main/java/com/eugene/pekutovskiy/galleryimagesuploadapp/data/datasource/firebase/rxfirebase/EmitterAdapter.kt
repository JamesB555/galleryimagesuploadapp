package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.rxfirebase

import io.reactivex.ObservableEmitter
import io.reactivex.SingleEmitter

interface EmitterAdapter<T> {

    fun onSuccess(item: T)

    fun onFailure(ex: Exception)

    fun onComplete() {
        // no opt
    }

    fun isDisposed(): Boolean
}

class SingleEmitterAdapter<T>(
    private val emitter: SingleEmitter<T>
) : EmitterAdapter<T> {

    override fun onSuccess(item: T) {
        emitter.onSuccess(item)
    }

    override fun onFailure(ex: Exception) {
        emitter.onError(ex)
    }

    override fun isDisposed(): Boolean = emitter.isDisposed
}

class ObservableEmitterAdapter<T>(
    private val emitter: ObservableEmitter<T>
) : EmitterAdapter<T> {

    override fun onSuccess(item: T) {
        emitter.onNext(item)
    }

    override fun onFailure(ex: Exception) {
        emitter.onError(ex)
    }

    override fun onComplete() {
        emitter.onComplete()
    }

    override fun isDisposed(): Boolean = emitter.isDisposed

}