package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class FirebaseStorageRef(@Suppress("unused") val type: Type) {
    enum class Type {
        ROOT, DOWNLOAD, UPLOAD
    }
}