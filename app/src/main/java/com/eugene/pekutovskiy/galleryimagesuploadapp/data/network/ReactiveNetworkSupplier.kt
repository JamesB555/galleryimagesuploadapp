package com.eugene.pekutovskiy.galleryimagesuploadapp.data.network

import android.content.Context
import com.github.pwittchen.reactivenetwork.library.rx2.Connectivity
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import javax.inject.Inject

class ReactiveNetworkSupplier @Inject constructor(
    private val context: Context
) {

    fun observe(): Flowable<Connectivity> =
        ReactiveNetwork.observeNetworkConnectivity(context)
            .toFlowable(BackpressureStrategy.LATEST)
}