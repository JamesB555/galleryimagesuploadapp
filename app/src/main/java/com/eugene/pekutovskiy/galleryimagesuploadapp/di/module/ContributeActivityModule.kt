package com.eugene.pekutovskiy.galleryimagesuploadapp.di.module

import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.MainActivity
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.GalleryModule
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage.UploadImageModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ContributeActivityModule {

    @ContributesAndroidInjector(modules = [GalleryModule::class, UploadImageModule::class])
    abstract fun contributesMainActivity(): MainActivity

}