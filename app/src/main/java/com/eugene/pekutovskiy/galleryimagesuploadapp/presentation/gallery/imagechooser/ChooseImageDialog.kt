package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.eugene.pekutovskiy.galleryimagesuploadapp.databinding.ChoosePictureDialogViewBinding

class ChooseImageDialog : DialogFragment() {

    private var callback: ChoosePickOptionCallback? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = parentFragment as? ChoosePickOptionCallback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return ChoosePictureDialogViewBinding.inflate(
            inflater, container, false
        ).apply {
            pickFromGallery.setOnClickListener {
                onOptionHasChosen(PickupOption.FROM_GALLERY)
            }
            takeFromCamera.setOnClickListener {
                onOptionHasChosen(PickupOption.FROM_CAMERA)
            }
        }.root
    }

    private fun onOptionHasChosen(pickupOption: PickupOption) {
        callback?.onOptionHasChosen(pickupOption)
        dismiss()
    }

    enum class PickupOption {
        FROM_GALLERY, FROM_CAMERA
    }

    interface ChoosePickOptionCallback {
        fun onOptionHasChosen(pickupOption: PickupOption)
    }

    companion object {
        const val TAG = "ChoosePictureDialog"
        fun newInstance() = ChooseImageDialog()
    }
}