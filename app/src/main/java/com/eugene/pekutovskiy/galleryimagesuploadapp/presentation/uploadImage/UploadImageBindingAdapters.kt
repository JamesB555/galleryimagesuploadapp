package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage

import android.view.View
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage.UploadImageViewModel


@BindingAdapter(value = ["showProgressIfLoading"])
fun showProgressIfLoading(progressBar: ProgressBar?, uiEvent: UploadImageViewModel.UiEvent?) {
    val isLoading = uiEvent != null && uiEvent is UploadImageViewModel.UiEvent.Loading
    progressBar?.visibility = if (isLoading) View.VISIBLE else View.GONE
}

@BindingAdapter(value = ["showIfNotLoading"])
fun showIfNotLoading(view: View?, uiEvent: UploadImageViewModel.UiEvent?) {
    val isLoading = uiEvent != null && uiEvent is UploadImageViewModel.UiEvent.Loading
    view?.visibility = if (isLoading) View.INVISIBLE else View.VISIBLE
}