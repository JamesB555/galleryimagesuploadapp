package com.eugene.pekutovskiy.galleryimagesuploadapp.data.network.exception

class NoNetworkException : Exception()