package com.eugene.pekutovskiy.galleryimagesuploadapp.util

import androidx.lifecycle.Observer

class NullCheckObserver<T>(
    val onSuccess: (T) -> Unit
) : Observer<T> {
    override fun onChanged(t: T?) {
        if (t != null) {
            onSuccess(t)
        }
    }
}