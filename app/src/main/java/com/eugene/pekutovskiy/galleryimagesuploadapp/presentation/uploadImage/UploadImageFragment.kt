package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.eugene.pekutovskiy.galleryimagesuploadapp.R
import com.eugene.pekutovskiy.galleryimagesuploadapp.annotations.OpenForTesting
import com.eugene.pekutovskiy.galleryimagesuploadapp.databinding.UploadPictureFragmentBinding
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.NetworkInfo
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.provideViewModel
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.setDisplayHomeAsUpEnabled
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.showError
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.showSnackbar
import com.google.android.material.snackbar.Snackbar
import com.theartofdev.edmodo.cropper.CropImage
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class UploadImageFragment : Fragment() {
    @OpenForTesting
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @VisibleForTesting
    lateinit var uploadImageViewModel: UploadImageViewModel
    private lateinit var binding: UploadPictureFragmentBinding
    private var imageUri: Uri? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        uploadImageViewModel = provideViewModel(viewModelFactory)
        return UploadPictureFragmentBinding.inflate(inflater, container, false).apply {
            binding = this
            viewModel = uploadImageViewModel
            lifecycleOwner = this@UploadImageFragment
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = getString(R.string.new_post)
        requireActivity().setDisplayHomeAsUpEnabled(true)
        imageUri = arguments?.getParcelable(PIC_URI)
        binding.uri = imageUri

        uploadImageViewModel.uiEventLiveData.observe(
            viewLifecycleOwner,
            Observer(::handleUiEvent)
        )

        uploadImageViewModel.connectivityStatusLiveData.observe(
            viewLifecycleOwner, Observer {
                if (it == NetworkInfo.NO_NETWORK) {
                    requireActivity().showError(null, getString(R.string.no_network_error_msg))
                }
            }
        )
    }

    private fun handleUiEvent(uiEvent: UploadImageViewModel.UiEvent) {
        when (uiEvent) {
            is UploadImageViewModel.UiEvent.UploadImageClicked -> {
                binding.uploadButton.isEnabled = false
                imageUri?.let(uploadImageViewModel::uploadImage)
            }

            is UploadImageViewModel.UiEvent.ImageUploaded -> {
                requireActivity().showSnackbar(
                    getString(R.string.image_has_been_uploaded),
                    onDismissEvent = {
                        activity?.onBackPressed()
                    },
                    durationFlag = Snackbar.LENGTH_SHORT
                )
            }

            is UploadImageViewModel.UiEvent.Error -> {
                binding.uploadButton.isEnabled = true
                requireActivity().showError(uiEvent.throwable)
            }

            is UploadImageViewModel.UiEvent.ProcessImage -> {
                startCropActivity()
            }
        }
    }

    private fun startCropActivity() {
        imageUri?.let {
            CropImage.activity(it)
                .setActivityTitle(getString(R.string.process_image))
                .start(requireContext(), this)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            val result = CropImage.getActivityResult(data)
            when (resultCode) {
                RESULT_OK -> {
                    result.uri?.let {
                        binding.uri = it
                        imageUri = it
                    }
                }
                CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE -> {
                    requireActivity().showError(result.error)
                }
            }
        }
    }

    companion object {
        const val PIC_URI = "pic_uri"
        fun newInstance() = UploadImageFragment()

        fun createBundle(uri: Uri) = Bundle().apply {
            putParcelable(PIC_URI, uri)
        }
    }
}