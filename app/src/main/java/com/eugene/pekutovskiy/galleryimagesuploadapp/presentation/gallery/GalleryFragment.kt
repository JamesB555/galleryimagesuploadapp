package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.eugene.pekutovskiy.galleryimagesuploadapp.R
import com.eugene.pekutovskiy.galleryimagesuploadapp.annotations.OpenForTesting
import com.eugene.pekutovskiy.galleryimagesuploadapp.databinding.GalleryFragmentViewBinding
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.FragmentFactory
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.NetworkInfo
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.UPLOAD_IMAGE_FRAGMENT
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.GalleryViewModel.UiEvent
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.adapter.GalleryListAdapter
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser.ChooseImageDialog
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser.ImageChooserDelegate
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage.UploadImageFragment
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.*
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class GalleryFragment : Fragment(), ChooseImageDialog.ChoosePickOptionCallback {

    @OpenForTesting
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var galleryListAdapter: GalleryListAdapter
    @Inject
    lateinit var imageChooserDelegate: ImageChooserDelegate
    @Inject
    lateinit var fragmentFactory: FragmentFactory
    @VisibleForTesting
    lateinit var galleryViewModel: GalleryViewModel
    private lateinit var binding: GalleryFragmentViewBinding

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        galleryViewModel = provideViewModel(viewModelFactory)
        return GalleryFragmentViewBinding.inflate(inflater, container, false).apply {
            binding = this
            setupViews()
            viewModel = galleryViewModel
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        galleryViewModel.uiEventLiveData.observe(
            viewLifecycleOwner,
            NullCheckObserver(::handleUiEvent)
        )
        galleryViewModel.connectivityStatusLiveData.observe(
            viewLifecycleOwner, Observer {
                if (it == NetworkInfo.NO_NETWORK) {
                    requireActivity().showError(null, getString(R.string.no_network_error_msg))
                }
            }
        )

        galleryViewModel.populateGallery()
    }

    // ChoosePictureDialog callback
    override fun onOptionHasChosen(pickupOption: ChooseImageDialog.PickupOption) {
        when (pickupOption) {
            ChooseImageDialog.PickupOption.FROM_GALLERY -> {
                imageChooserDelegate.pickFromGallery(this)
            }
            ChooseImageDialog.PickupOption.FROM_CAMERA -> {
                imageChooserDelegate.takeFromCamera(this)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != RESULT_OK) {
            Timber.w("taking pic error")
            return
        }

        when (requestCode) {
            ImageChooserDelegate.REQUEST_IMAGE_CAPTURE -> {
                val photoUri = imageChooserDelegate.photoUri
                photoUri?.let(::navigateToUploadPicFragment)
            }

            ImageChooserDelegate.REQUEST_PICK_FROM_GALLERY -> {
                val selectedImgUri = data?.data
                selectedImgUri?.let(::navigateToUploadPicFragment)
            }

            else -> throw IllegalArgumentException("Unknown request code")
        }
    }

    private fun navigateToUploadPicFragment(uri: Uri) {
        galleryViewModel.invalidateUiEventListener()
        val fragment = fragmentFactory.createFragment(
            UPLOAD_IMAGE_FRAGMENT,
            UploadImageFragment.createBundle(uri)
        )
        requireActivity().loadFragment(fragment, addToBackStack = true)
    }

    private fun setupViews() {
        requireActivity().title = getString(R.string.gallery)
        requireActivity().setDisplayHomeAsUpEnabled(false)

        galleryListAdapter.onClickListener = {
            Toast.makeText(
                requireContext(),
                getString(R.string.item_clicked_stub),
                Toast.LENGTH_SHORT
            ).show()
        }

        binding.recyclerView.apply {
            adapter = galleryListAdapter
            layoutManager = GridLayoutManager(requireContext(), SPAN_COUNT)
        }
        binding.swipeRefreshLayout.setOnRefreshListener(galleryViewModel::onSwipeRefresh)
    }

    private fun handleUiEvent(uiEvent: UiEvent) {
        binding.swipeRefreshLayout.isRefreshing = false
        when (uiEvent) {
            is UiEvent.GalleryUpdate -> {
                galleryListAdapter.submitList(uiEvent.pics)
            }
            is UiEvent.Error -> {
                requireActivity().showError(uiEvent.throwable)
            }
            is UiEvent.ShowChooseImageDialog -> {
                val dialog = ChooseImageDialog.newInstance()
                dialog.show(childFragmentManager, ChooseImageDialog.TAG)
            }
        }
    }

    companion object {
        private const val SPAN_COUNT = 2
    }
}