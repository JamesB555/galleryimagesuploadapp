package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.network.ReactiveNetworkSupplier
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.rx.SchedulersFacade
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Provider

interface ConnectivityStatusProviderDelegate {
    val connectivityStatusLiveData: LiveData<NetworkInfo>
    fun isConnected(): Boolean
    fun cleanup()
}

class ConnectivityStatusProviderDelegateImpl @Inject constructor(
    reactiveNetwork: Provider<ReactiveNetworkSupplier>,
    schedulersFacade: SchedulersFacade
) : ConnectivityStatusProviderDelegate {

    private var disposable: Disposable? = null

    override val connectivityStatusLiveData = MutableLiveData<NetworkInfo>()

    init {
        disposable = reactiveNetwork.get().observe()
            .doOnError {
                connectivityStatusLiveData.value = NetworkInfo.NO_NETWORK
            }
            /*  handling all possible errors in network status observer's chain is crucial for the app,
            but in this sample we'll keep this plain solution with retries count just for simplicity */
            .retry(RETRIES_COUNT)
            .subscribeOn(schedulersFacade.io())
            .observeOn(schedulersFacade.main())
            .subscribe(
                {
                    connectivityStatusLiveData.value = if (it.available()) {
                        NetworkInfo.CONNECTED
                    } else {
                        NetworkInfo.NO_NETWORK
                    }

                }, Timber::e
            )
    }

    override fun isConnected() = connectivityStatusLiveData.value?.let {
        it == NetworkInfo.CONNECTED
    } ?: false

    override fun cleanup() {
        disposable?.dispose()
    }

    companion object {
        const val RETRIES_COUNT = 3L
    }
}

enum class NetworkInfo {
    CONNECTED, NO_NETWORK
}