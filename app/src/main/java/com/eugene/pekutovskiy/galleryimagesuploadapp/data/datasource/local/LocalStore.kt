package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.local

import android.net.Uri
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*

interface LocalStore {

    fun storeUris(uris: List<Uri>): Completable

    fun getImages(): Single<Optional<List<Uri>>>
}