package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage

import androidx.lifecycle.ViewModel
import com.eugene.pekutovskiy.galleryimagesuploadapp.di.factory.mapkey.VmKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class UploadImageModule {

    @ContributesAndroidInjector
    abstract fun contributesUploadPictureFragment(): UploadImageFragment

    @Binds
    @IntoMap
    @VmKey(UploadImageViewModel::class)
    abstract fun bindsUploadImageViewModel(
        vm: UploadImageViewModel
    ): ViewModel

}