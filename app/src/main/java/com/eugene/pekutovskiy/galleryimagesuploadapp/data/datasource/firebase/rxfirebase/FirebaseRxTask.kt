package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.rxfirebase

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import io.reactivex.ObservableEmitter
import io.reactivex.SingleEmitter

class FirebaseRxTask<T> private constructor(
    private val firebaseTask: Task<T>,
    private val emitterAdapter: EmitterAdapter<T>
) : OnSuccessListener<T>, OnFailureListener, OnCompleteListener<T> {

    init {
        firebaseTask.addOnSuccessListener(this)
        firebaseTask.addOnFailureListener(this)
        firebaseTask.addOnCompleteListener(this)
    }

    override fun onSuccess(item: T) {
        if (firebaseTask.isSuccessful) {
            emitterAdapter.onSuccess(item)
        } else {
            emitterAdapter.onFailure(
                firebaseTask.exception
                /* generic error directed downstream as we are not interested
                   in some particular type for this small sample*/
                    ?: RuntimeException("Unknown Firebase error")
            )
        }
    }

    override fun onFailure(ex: Exception) {
        if (!emitterAdapter.isDisposed()) {
            emitterAdapter.onFailure(ex)
        }
    }

    override fun onComplete(result: Task<T>) {
        emitterAdapter.onComplete()
    }

    //factory methods
    companion object {
        fun <T> createSingleTask(
            emitter: SingleEmitter<T>,
            firebaseTask: Task<T>
        ) =
            FirebaseRxTask(
                firebaseTask,
                SingleEmitterAdapter(
                    emitter
                )
            )

        fun <T> createTask(
            emitter: ObservableEmitter<T>,
            firebaseTask: Task<T>
        ) =
            FirebaseRxTask(
                firebaseTask,
                ObservableEmitterAdapter(
                    emitter
                )
            )

        fun <T> createTestTask(
            emitter: ObservableEmitterAdapter<T>,
            firebaseTask: Task<T>
        ) = FirebaseRxTask(firebaseTask, emitter)
    }
}