package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage

import android.net.Uri
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.eugene.pekutovskiy.galleryimagesuploadapp.annotations.OpenForTesting
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.network.exception.NoNetworkException
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.UploadImageUseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.ConnectivityStatusProviderDelegate
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.ConnectivityStatusProviderDelegateImpl
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.rx.SchedulersFacade
import io.reactivex.disposables.Disposable
import javax.inject.Inject

@OpenForTesting
class UploadImageViewModel @Inject constructor(
    connectivityStatusProvider: ConnectivityStatusProviderDelegateImpl,
    private val uploadImageUseCase: UploadImageUseCase,
    private val schedulersFacade: SchedulersFacade
) : ViewModel(), ConnectivityStatusProviderDelegate by connectivityStatusProvider {

    val uiEventLiveData = MutableLiveData<UiEvent>()
    private var uploadImageDisposable: Disposable? = null

    fun onUploadImageClick() {
        uiEventLiveData.value = UiEvent.UploadImageClicked
    }

    fun onProcessImageClick() {
        uiEventLiveData.value = UiEvent.ProcessImage
    }

    fun uploadImage(uri: Uri) {
        if (!isConnected()) {
            uiEventLiveData.value = UiEvent.Error(NoNetworkException())
            return
        }
        uiEventLiveData.value = UiEvent.Loading
        uploadImageDisposable = uploadImageUseCase.execute(uri)
            .subscribeOn(schedulersFacade.io())
            .observeOn(schedulersFacade.main())
            .subscribe(
                {
                    uiEventLiveData.value = UiEvent.ImageUploaded
                },
                {
                    uiEventLiveData.value = UiEvent.Error(it)
                }
            )
    }

    override fun onCleared() {
        cleanup()
        uploadImageDisposable?.dispose()
        super.onCleared()
    }

    sealed class UiEvent {
        object Loading : UiEvent()
        object ImageUploaded : UiEvent()
        class Error(val throwable: Throwable) : UiEvent()
        object ProcessImage : UiEvent()
        object UploadImageClicked : UiEvent()
    }
}