package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery

import androidx.lifecycle.ViewModel
import com.eugene.pekutovskiy.galleryimagesuploadapp.di.factory.mapkey.VmKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class GalleryModule {

    @ContributesAndroidInjector
    abstract fun contributesGalleryFragment(): GalleryFragment

    @Binds
    @IntoMap
    @VmKey(GalleryViewModel::class)
    abstract fun bindsGalleryViewModel(
        vm: GalleryViewModel
    ): ViewModel
}