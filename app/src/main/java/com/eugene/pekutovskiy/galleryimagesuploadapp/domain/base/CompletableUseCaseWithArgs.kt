package com.eugene.pekutovskiy.galleryimagesuploadapp.domain.base

import io.reactivex.Completable

interface CompletableUseCaseWithArgs<T> {

    fun execute(arg: T): Completable
}