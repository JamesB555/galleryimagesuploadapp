package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase

import android.net.Uri
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.DataSource
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.di.FirebaseStorageRef
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.di.FirebaseStorageRef.Type
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.rxfirebase.FirebaseRxTask
import com.google.firebase.storage.ListResult
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Provider

class FirebaseDataSource @Inject constructor(
    @FirebaseStorageRef(Type.DOWNLOAD)
    private val downloadStorageRef: StorageReference,
    @FirebaseStorageRef(Type.UPLOAD)
    private val uploadStorageRefProvider: Provider<StorageReference>
) : DataSource {

    override fun uploadImage(uri: Uri): Completable {
        return Single.create<UploadTask.TaskSnapshot> { emitter ->
            FirebaseRxTask.createSingleTask(
                emitter,
                uploadStorageRefProvider.get().putFile(uri)
            )
        }.ignoreElement()
    }

    override fun getImages(): Single<List<Uri>> {
        return getItemsStorageReference(downloadStorageRef)
            .flatMapObservable {
                Observable.fromIterable(it)
            }.flatMap {
                Observable.create<Uri> { emitter ->
                    FirebaseRxTask.createTask(emitter, it.downloadUrl)
                }
            }.toList()
    }

    private fun getItemsStorageReference(childRef: StorageReference) =
        Single.create<ListResult> { emitter ->
            FirebaseRxTask.createSingleTask(emitter, childRef.listAll())
        }.map {
            it.items
        }
}