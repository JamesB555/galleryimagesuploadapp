package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.local

import android.net.Uri
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*
import javax.inject.Inject

class SimpleCache @Inject constructor() : LocalStore {

    private var cache: List<Uri>? = null

    override fun storeUris(uris: List<Uri>) = Completable.fromAction {
        cache = uris
    }

    override fun getImages() = Single.just(
        Optional.ofNullable(cache)
    )
}