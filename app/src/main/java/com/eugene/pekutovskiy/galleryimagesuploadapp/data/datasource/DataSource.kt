package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource

import android.net.Uri
import io.reactivex.Completable
import io.reactivex.Single

interface DataSource {

    fun uploadImage(uri: Uri): Completable

    fun getImages(): Single<List<Uri>>
}