package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.di

import com.eugene.pekutovskiy.galleryimagesuploadapp.BuildConfig
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.di.FirebaseStorageRef.Type
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import dagger.Module
import dagger.Provides
import java.util.*

@Module
class FirebaseModule {

    @Provides
    @FirebaseStorageRef(Type.ROOT)
    fun provideBaseStorageReference() = FirebaseStorage.getInstance().reference

    @Provides
    @FirebaseStorageRef(Type.DOWNLOAD)
    fun provideDownloadStorageReference(
        @FirebaseStorageRef(Type.ROOT)
        baseRef: StorageReference
    ) = baseRef.child(BuildConfig.IMAGES_FIREBASE_FOLDER_NAME)

    @Provides
    @FirebaseStorageRef(Type.UPLOAD)
    fun provideUploadStorageReference(
        @FirebaseStorageRef(Type.ROOT)
        baseRef: StorageReference
    ): StorageReference {
        val stringPath = BuildConfig.IMAGES_FIREBASE_FOLDER_NAME + UUID.randomUUID() + ".jpg"
        return baseRef.child(stringPath)
    }
}