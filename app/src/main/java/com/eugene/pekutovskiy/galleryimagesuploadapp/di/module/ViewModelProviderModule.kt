package com.eugene.pekutovskiy.galleryimagesuploadapp.di.module

import androidx.lifecycle.ViewModelProvider
import com.eugene.pekutovskiy.galleryimagesuploadapp.di.factory.ViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class ViewModelProviderModule {

    @Binds
    internal abstract fun bindViewModelFactory(
        factory: ViewModelFactory
    ): ViewModelProvider.Factory
}