package com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo

import android.net.Uri
import io.reactivex.Completable
import io.reactivex.Single

interface ImagesRepository {

    fun fetchImages(): Single<List<Uri>>

    fun forceRefreshImages(): Single<List<Uri>>

    fun uploadImage(uri: Uri): Completable
}