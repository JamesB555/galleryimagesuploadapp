package com.eugene.pekutovskiy.galleryimagesuploadapp.data

import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.DataSource
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.FirebaseDataSource
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.local.LocalStore
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.local.SimpleCache
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo.ImagesRepository
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo.ImagesRepositoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DataSourceModule {

    @Binds
    abstract fun bindsLocalStore(simpleCache: SimpleCache): LocalStore

    @Binds
    abstract fun bindsRemoteDataSource(firebaseDataSource: FirebaseDataSource): DataSource

    @Binds
    @Singleton
    abstract fun bindsImagesRepository(repo: ImagesRepositoryImpl): ImagesRepository
}