package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common

import android.os.Bundle
import androidx.annotation.IntDef
import androidx.fragment.app.Fragment
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.GalleryFragment
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage.UploadImageFragment
import javax.inject.Inject
import javax.inject.Singleton

const val GALLERY_FRAGMENT = 1
const val UPLOAD_IMAGE_FRAGMENT = 2

@IntDef(GALLERY_FRAGMENT, UPLOAD_IMAGE_FRAGMENT)
@Retention(AnnotationRetention.SOURCE)
internal annotation class FragmentId

@Singleton
class FragmentFactory @Inject constructor() {
    fun createFragment(@FragmentId fragmentId: Int, bundle: Bundle? = null): Fragment {
        return when (fragmentId) {
            GALLERY_FRAGMENT -> GalleryFragment()
            UPLOAD_IMAGE_FRAGMENT -> UploadImageFragment.newInstance()
            else -> throw IllegalArgumentException("unsupported fragment id")
        }.also {
            if (bundle != null) {
                it.arguments = bundle
            }
        }
    }
}