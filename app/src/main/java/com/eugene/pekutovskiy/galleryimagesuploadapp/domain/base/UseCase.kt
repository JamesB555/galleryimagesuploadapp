package com.eugene.pekutovskiy.galleryimagesuploadapp.domain.base

import io.reactivex.Single

interface UseCase<T> {
    fun execute(): Single<T>
}