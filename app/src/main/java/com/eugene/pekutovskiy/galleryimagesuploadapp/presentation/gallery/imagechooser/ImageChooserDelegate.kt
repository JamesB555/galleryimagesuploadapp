package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.eugene.pekutovskiy.galleryimagesuploadapp.annotations.OpenForTesting
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

@OpenForTesting
class ImageChooserDelegate @Inject constructor() {

    var photoUri: Uri? = null

    fun takeFromCamera(fragment: Fragment) {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        takePictureIntent.resolveActivity(fragment.requireContext().packageManager)?.also {
            val context = fragment.requireContext()
            val photoFile: File? = try {
                createImageFile(context)
            } catch (ex: IOException) {
                null
            }

            photoFile?.let {
                val uri: Uri = FileProvider.getUriForFile(
                    context,
                    context.packageName,
                    it
                )

                photoUri = uri

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
                fragment.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    fun pickFromGallery(fragment: Fragment) {
        val galleryIntent = Intent(
            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        ).apply {
            putExtra(GALLERY_INTENT_EXTRA, true)
        }
        fragment.startActivityForResult(
            galleryIntent,
            REQUEST_PICK_FROM_GALLERY
        )
    }

    private fun createImageFile(context: Context): File {
        val timeStamp: String =
            SimpleDateFormat(SDF_PATTERN, Locale.getDefault()).format(Date())
        val imagePath = File(
            context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            FOLDER_PATH_IMAGES
        )
        if (!imagePath.exists()) {
            imagePath.mkdirs()
        }
        return File.createTempFile(
            "$TAKE_FROM_CAM_FILE_PREFIX${timeStamp}_",
            TAKE_FROM_CAM_FILE_SUFFIX,
            imagePath
        )
    }

    companion object {
        const val REQUEST_IMAGE_CAPTURE = 1
        const val REQUEST_PICK_FROM_GALLERY = 2

        private const val FOLDER_PATH_IMAGES = "spark_images"
        private const val SDF_PATTERN = "yyyyMMdd_HHmmss"
        private const val TAKE_FROM_CAM_FILE_SUFFIX = ".jpg"
        private const val TAKE_FROM_CAM_FILE_PREFIX = "JPEG_"
        private const val GALLERY_INTENT_EXTRA = "return-data"
    }
}