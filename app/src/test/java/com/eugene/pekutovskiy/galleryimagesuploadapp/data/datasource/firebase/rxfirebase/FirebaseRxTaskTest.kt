package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.rxfirebase

import com.google.android.gms.tasks.Task
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class FirebaseRxTaskTest {

    @Mock
    lateinit var firebaseTask: Task<Any>
    @Mock
    lateinit var emitterAdapter: ObservableEmitterAdapter<Any>
    private val exception = Exception()
    private lateinit var underTest: FirebaseRxTask<Any>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        underTest = FirebaseRxTask.createTestTask(emitterAdapter, firebaseTask)

        verify(firebaseTask).addOnSuccessListener(underTest)
        verify(firebaseTask).addOnFailureListener(underTest)
        verify(firebaseTask).addOnCompleteListener(underTest)
    }

    @Test
    fun `test onSuccess when task result is successful`() {
        val resultData = Any()
        val argumentCaptor = argumentCaptor<Any>()
        ArrangeBuilder().withTaskResult(isSuccessful = true)

        underTest.onSuccess(resultData)

        verify(emitterAdapter).onSuccess(argumentCaptor.capture())
        assertThat(argumentCaptor.firstValue, `is`(equalTo(resultData)))
    }

    @Test
    fun `test onSuccess when task result is unsuccessful`() {
        val resultData = Any()
        ArrangeBuilder()
            .withTaskResult(isSuccessful = false)
            .withFirebaseException(exception)

        underTest.onSuccess(resultData)

        verify(emitterAdapter).onFailure(exception)
    }

    @Test
    fun `test onFailure when emitter is disposed`() {
        ArrangeBuilder().withDisposedEmitter(isDisposed = true)
        underTest.onFailure(exception)
        verify(emitterAdapter, never()).onFailure(exception)
    }

    @Test
    fun `test onFailure when emitter is not disposed`() {
        ArrangeBuilder().withDisposedEmitter(isDisposed = false)
        underTest.onFailure(exception)
        verify(emitterAdapter).onFailure(exception)
    }

    @Test
    fun `test onComplete`() {
        underTest.onComplete(firebaseTask)
        verify(emitterAdapter).onComplete()
    }

    private inner class ArrangeBuilder {

        fun withTaskResult(isSuccessful: Boolean): ArrangeBuilder {
            whenever(firebaseTask.isSuccessful).thenReturn(isSuccessful)
            return this
        }

        fun withFirebaseException(ex: Exception): ArrangeBuilder {
            whenever(firebaseTask.exception).thenReturn(ex)
            return this
        }

        fun withDisposedEmitter(isDisposed: Boolean): ArrangeBuilder {
            whenever(emitterAdapter.isDisposed()).thenReturn(isDisposed)
            return this
        }
    }
}