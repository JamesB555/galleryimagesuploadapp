package com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo

import android.net.Uri
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.DataSource
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.local.LocalStore
import com.eugene.pekutovskiy.testutil.TestDataUtil
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.*

class ImagesRepositoryImplTest {

    @Mock
    lateinit var remoteDataSource: DataSource
    @Mock
    lateinit var localDataSource: LocalStore

    private val urisList: List<Uri> = TestDataUtil.GALLERY_DATA_URI
    private lateinit var underTest: ImagesRepositoryImpl

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        underTest = ImagesRepositoryImpl(remoteDataSource, localDataSource)
    }

    @Test
    fun `verify that data is fetched from cache if it's not empty`() {
        underTest = ImagesRepositoryImpl(remoteDataSource, localDataSource)
        ArrangeBuilder()
            .withCachedData(urisList)

        underTest.fetchImages()
            .test()
            .assertComplete()
            .assertNoErrors()
            .assertValue(urisList)
            .dispose()

        verify(localDataSource).getImages()
        verifyZeroInteractions(remoteDataSource)
    }

    @Test
    fun `test fetching images if cache is empty`() {
        ArrangeBuilder()
            .withCachedData(null)
            .withApiResponseData(urisList)
            .stubComparableSelectors()
            .completeStoreUrisMethod()

        underTest.fetchImages()
            .test()
            .assertNoErrors()
            .assertComplete()
            .dispose()

        val inOrder = inOrder(localDataSource, remoteDataSource)
        inOrder.run {
            verify(localDataSource).getImages()
            verify(remoteDataSource).getImages()
            verify(localDataSource).storeUris(urisList)
        }
    }

    @Test
    fun `test uploadImage`() {
        val uri = TestDataUtil.GALLERY_DATA_URI[0]
        ArrangeBuilder()
            .completeUploadImageWithUri(uri)
            .withApiResponseData(urisList)
            .stubComparableSelectors()

        underTest.uploadImage(uri)
            .test()
            .assertComplete()
            .assertNoErrors()
            .dispose()

        val inOrder = inOrder(localDataSource, remoteDataSource)
        inOrder.run {
            verify(remoteDataSource, times(1)).uploadImage(uri)
            verify(remoteDataSource).getImages()
            verify(localDataSource).storeUris(urisList)
        }
    }

    @Test
    fun `test forceRefreshImages`() {
        ArrangeBuilder()
            .withApiResponseData(urisList)
            .stubComparableSelectors()
            .completeStoreUrisMethod()

        underTest.forceRefreshImages()
            .test()
            .assertComplete()
            .assertNoErrors()
            .dispose()

        verify(localDataSource, never()).getImages()

        val inOrder = inOrder(localDataSource, remoteDataSource)
        inOrder.run {
            verify(remoteDataSource).getImages()
            verify(localDataSource).storeUris(urisList)
        }
    }

    private inner class ArrangeBuilder {
        fun withCachedData(cachedData: List<Uri>?): ArrangeBuilder {
            val optional = Optional.ofNullable(cachedData)
            whenever(localDataSource.getImages()).thenReturn(
                Single.just(optional)
            )
            return this
        }

        fun withApiResponseData(urisList: List<Uri>): ArrangeBuilder {
            whenever(remoteDataSource.getImages()).thenReturn(
                Single.just(urisList)
            )
            return this
        }

        fun completeUploadImageWithUri(uri: Uri): ArrangeBuilder {
            whenever(remoteDataSource.uploadImage(uri)).thenReturn(Completable.complete())
            return this
        }

        fun completeStoreUrisMethod(): ArrangeBuilder {
            whenever(localDataSource.storeUris(urisList)).thenReturn(Completable.complete())
            return this
        }

        fun stubComparableSelectors(): ArrangeBuilder {
            whenever(urisList[0].toString()).thenReturn(TestDataUtil.GALLERY_DATA_STR[0])
            whenever(urisList[1].toString()).thenReturn(TestDataUtil.GALLERY_DATA_STR[1])
            return this
        }
    }
}