package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase

import android.net.Uri
import com.eugene.pekutovskiy.testutil.TestDataUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.ListResult
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.nhaarman.mockitokotlin2.*
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.*
import javax.inject.Provider


class FirebaseDataSourceTest {

    @Mock
    lateinit var downloadStorageRef: StorageReference
    @Mock
    lateinit var uploadStorageRefProvider: Provider<StorageReference>
    private val uri: Uri = TestDataUtil.GALLERY_DATA_URI[0]
    lateinit var underTest: FirebaseDataSource

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        underTest = FirebaseDataSource(downloadStorageRef, uploadStorageRefProvider)
    }

    @Test
    fun `test uploadImage`() {
        val uploadStorageReference: StorageReference = mock()
        val uploadTask: UploadTask = mock()
        val taskSnapshot: UploadTask.TaskSnapshot = mock()
        val onSuccessCaptor =
            argumentCaptor<OnSuccessListener<UploadTask.TaskSnapshot>>()

        ArrangeBuilder()
            .withUploadStorageReference(uploadStorageReference)
            .withUploadTask(uploadStorageReference, uploadTask)
            .withOnSuccessListener(uploadTask, onSuccessCaptor)
            .withTaskStatus(task = uploadTask, isSuccessful = true)

        val testObserver = underTest
            .uploadImage(uri)
            .test()
        onSuccessCaptor.lastValue.onSuccess(taskSnapshot)

        verify(uploadStorageReference).putFile(uri)

        testObserver
            .assertComplete()
            .assertNoErrors()
            .dispose()
    }

    @Test
    fun `test getImages`() {
        // list result task mocks
        val listResult: ListResult = mock()
        val listResultTask: Task<ListResult> = mock()
        val item: StorageReference = mock()
        val itemsList = Collections.singletonList(item)
        // uri task mocks
        val uriTask: Task<Uri> = mock()
        //captors
        val onSuccessListResultCaptor = argumentCaptor<OnSuccessListener<ListResult>>()
        val onSuccessUriTaskCaptor = argumentCaptor<OnSuccessListener<Uri>>()
        val onCompleteUriTaskCaptor = argumentCaptor<OnCompleteListener<Uri>>()

        // list result task setup
        ArrangeBuilder()
            .withListAllTask(downloadStorageRef, listResultTask)
            .withListResult(listResultTask, listResult)
            .withListResultItems(listResult, itemsList)
            .withUriTask(item, uriTask)
            .withTaskStatus(task = listResultTask, isSuccessful = true)
            .withOnSuccessListener(listResultTask, onSuccessListResultCaptor)

        // uri task setup
        ArrangeBuilder()
            .withOnSuccessListener(uriTask, onSuccessUriTaskCaptor)
            .withOnCompleteListener(uriTask, onCompleteUriTaskCaptor)
            .withTaskStatus(task = uriTask, isSuccessful = true)

        val testObserver = underTest.getImages().test()

        onSuccessListResultCaptor.lastValue.onSuccess(listResult)
        onSuccessUriTaskCaptor.lastValue.onSuccess(uri)
        onCompleteUriTaskCaptor.lastValue.onComplete(uriTask)

        verify(downloadStorageRef).listAll()
        verify(item).downloadUrl

        testObserver
            .assertComplete()
            .assertNoErrors()
            .assertValueCount(1)
            .dispose()
    }

    private inner class ArrangeBuilder {

        fun withUploadStorageReference(storageRef: StorageReference): ArrangeBuilder {
            whenever(uploadStorageRefProvider.get()).thenReturn(storageRef)
            return this
        }

        fun withUploadTask(storageRef: StorageReference, uploadTask: UploadTask): ArrangeBuilder {
            whenever(storageRef.putFile(any(Uri::class.java))).thenReturn(uploadTask)
            return this
        }

        fun withListAllTask(
            storageRef: StorageReference,
            task: Task<ListResult>
        ): ArrangeBuilder {
            whenever(storageRef.listAll()).thenReturn(task)
            return this
        }

        fun <T> withOnSuccessListener(
            task: Task<T>,
            captor: KArgumentCaptor<OnSuccessListener<T>>
        ): ArrangeBuilder {
            whenever(task.addOnSuccessListener(captor.capture())).thenReturn(task)
            return this
        }

        fun <T> withOnCompleteListener(
            task: Task<T>,
            captor: KArgumentCaptor<OnCompleteListener<T>>
        ): ArrangeBuilder {
            whenever(task.addOnCompleteListener(captor.capture())).thenReturn(task)
            return this
        }

        fun withTaskStatus(task: Task<*>, isSuccessful: Boolean): ArrangeBuilder {
            whenever(task.isSuccessful).thenReturn(isSuccessful)
            return this
        }

        fun withListResult(task: Task<*>, listResult: ListResult): ArrangeBuilder {
            whenever(task.result).thenReturn(listResult)
            return this
        }

        fun withListResultItems(
            listResult: ListResult,
            items: List<StorageReference>
        ): ArrangeBuilder {
            whenever(listResult.items).thenReturn(items)
            return this
        }

        fun withUriTask(storageRef: StorageReference, result: Task<Uri>): ArrangeBuilder {
            whenever(storageRef.downloadUrl).thenReturn(result)
            return this
        }
    }
}