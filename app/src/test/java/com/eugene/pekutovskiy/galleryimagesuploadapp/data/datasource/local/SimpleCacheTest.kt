package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.local

import android.net.Uri
import com.nhaarman.mockitokotlin2.mock
import org.junit.Before
import org.junit.Test
import java.util.*

class SimpleCacheTest {

    private lateinit var underTest: SimpleCache

    private val urisList: List<Uri> = mock()

    @Before
    fun setup() {
        underTest = SimpleCache()
    }

    @Test
    fun `assert urisList is cached without errors`() {
        underTest.storeUris(urisList)
            .test()
            .assertComplete()
            .assertNoErrors()
    }

    @Test
    fun `assert empty optional is returned when items are not in cache`() {
        val expectedValue = Optional.empty<List<Uri>>()
        underTest.getImages()
            .test()
            .assertValue(expectedValue)
    }

    @Test
    fun `assert fetched expected value`() {
        val expectedValue = Optional.of(urisList)

        underTest.storeUris(urisList)
            .test()
            .assertComplete()

        underTest.getImages()
            .test()
            .assertValue(expectedValue)
    }

}