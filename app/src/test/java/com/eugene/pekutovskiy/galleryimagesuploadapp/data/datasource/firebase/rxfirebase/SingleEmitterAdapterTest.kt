package com.eugene.pekutovskiy.galleryimagesuploadapp.data.datasource.firebase.rxfirebase

import com.nhaarman.mockitokotlin2.argumentCaptor
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.verifyZeroInteractions
import io.reactivex.SingleEmitter
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class SingleEmitterAdapterTest {

    private lateinit var underTest: SingleEmitterAdapter<Any>
    private val emitter: SingleEmitter<Any> = mock()

    @Before
    fun setUp() {
        underTest = SingleEmitterAdapter(emitter)
    }

    @Test
    fun `test onSuccess`() {
        val item = Any()
        val argumentCaptor = argumentCaptor<Any>()
        underTest.onSuccess(item)
        verify(emitter).onSuccess(argumentCaptor.capture())
        assertThat(argumentCaptor.firstValue, `is`(equalTo(item)))
    }

    @Test
    fun `test onFailure`() {
        val exception = Exception()
        val argumentCaptor = argumentCaptor<Exception>()
        underTest.onFailure(exception)
        verify(emitter).onError(argumentCaptor.capture())
        assertThat(argumentCaptor.firstValue, `is`(equalTo(exception)))
    }

    @Test
    fun `test isDisposed`() {
        underTest.isDisposed()
        verify(emitter).isDisposed
    }

    @Test
    fun `test onComplete`() {
        underTest.onComplete()
        verifyZeroInteractions(emitter)
    }
}