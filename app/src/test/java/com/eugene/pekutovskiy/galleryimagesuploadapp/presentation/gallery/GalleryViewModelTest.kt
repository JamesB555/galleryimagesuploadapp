package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery

import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.FetchImagesUseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.ForceRefreshImagesUseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.base.UseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.ConnectivityStatusProviderDelegateImpl
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.GalleryViewModel.UiEvent
import com.eugene.pekutovskiy.testutil.TestDataUtil
import com.eugene.pekutovskiy.testutil.TestSchedulersFacade
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import junit.framework.Assert.assertNull
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsInstanceOf.instanceOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class GalleryViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private val fetchImagesUseCase: FetchImagesUseCase = mock()
    private val forceRefreshImagesUseCase: ForceRefreshImagesUseCase = mock()
    private val connectivityStatusProvider: ConnectivityStatusProviderDelegateImpl = mock()
    private val schedulers = TestSchedulersFacade()
    private lateinit var underTest: GalleryViewModel

    @Before
    fun setUp() {
        underTest = GalleryViewModel(
            connectivityStatusProvider,
            fetchImagesUseCase,
            forceRefreshImagesUseCase,
            schedulers
        )
    }

    @Test
    fun `test populateGallery if event success`() {
        ArrangeBuilder()
            .withUseCase(fetchImagesUseCase)

        underTest.populateGallery()

        verify(fetchImagesUseCase).execute()

        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.GalleryUpdate::class.java)
        )

        assertThat(
            (underTest.uiEventLiveData.value as UiEvent.GalleryUpdate).pics,
            equalTo(TestDataUtil.GALLERY_DATA_STR)
        )
    }

    @Test
    fun `test populateGallery if exception is thrown`() {
        val exception: Throwable = RuntimeException()
        ArrangeBuilder()
            .withExpectedException(exception, fetchImagesUseCase)

        underTest.populateGallery()

        verify(fetchImagesUseCase).execute()

        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.Error::class.java)
        )

        assertThat(
            (underTest.uiEventLiveData.value as UiEvent.Error).throwable,
            equalTo(exception)
        )
    }

    @Test
    fun `test onFabClicked`() {
        underTest.onFabClicked()

        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.ShowChooseImageDialog::class.java)
        )
    }

    @Test
    fun `test invalidateUiEventListener`() {
        underTest.invalidateUiEventListener()
        assertNull(underTest.uiEventLiveData.value)
    }

    @Test
    fun `test onSwipeRefresh if event success`() {
        ArrangeBuilder()
            .withUseCase(forceRefreshImagesUseCase)

        underTest.onSwipeRefresh()

        verify(forceRefreshImagesUseCase).execute()

        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.GalleryUpdate::class.java)
        )

        assertThat(
            (underTest.uiEventLiveData.value as UiEvent.GalleryUpdate).pics,
            equalTo(TestDataUtil.GALLERY_DATA_STR)
        )
    }

    @Test
    fun `test onSwipeRefresh if exception is thrown`() {
        val exception: Throwable = RuntimeException()
        ArrangeBuilder()
            .withExpectedException(exception, forceRefreshImagesUseCase)

        underTest.onSwipeRefresh()

        verify(forceRefreshImagesUseCase).execute()

        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.Error::class.java)
        )

        assertThat(
            (underTest.uiEventLiveData.value as UiEvent.Error).throwable,
            equalTo(exception)
        )
    }

    private inner class ArrangeBuilder {
        fun withUseCase(useCase: UseCase<List<Uri>>): ArrangeBuilder {
            whenever(useCase.execute()).thenReturn(
                Single.just(TestDataUtil.GALLERY_DATA_URI)
            )
            whenever(TestDataUtil.GALLERY_DATA_URI[0].toString()).thenReturn(TestDataUtil.GALLERY_DATA_STR[0])
            whenever(TestDataUtil.GALLERY_DATA_URI[1].toString()).thenReturn(TestDataUtil.GALLERY_DATA_STR[1])
            return this
        }

        fun withExpectedException(
            throwable: Throwable,
            useCase: UseCase<List<Uri>>
        ): ArrangeBuilder {
            whenever(useCase.execute()).thenReturn(Single.error(throwable))
            return this
        }
    }
}