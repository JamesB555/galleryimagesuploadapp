package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage

import android.net.Uri
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.network.exception.NoNetworkException
import com.eugene.pekutovskiy.galleryimagesuploadapp.domain.UploadImageUseCase
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.ConnectivityStatusProviderDelegateImpl
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage.UploadImageViewModel.UiEvent
import com.eugene.pekutovskiy.galleryimagesuploadapp.util.rx.SchedulersFacade
import com.eugene.pekutovskiy.testutil.TestDataUtil
import com.eugene.pekutovskiy.testutil.TestSchedulersFacade
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Completable
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class UploadImageViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    private val uploadImageUseCase: UploadImageUseCase = mock()
    private val connectivityStatusProvider: ConnectivityStatusProviderDelegateImpl = mock()
    private val observer: Observer<UiEvent> = mock()
    private val schedulersFacade: SchedulersFacade = TestSchedulersFacade()
    private val uri: Uri = TestDataUtil.GALLERY_DATA_URI[0]
    private lateinit var underTest: UploadImageViewModel

    @Before
    fun setUp() {
        underTest = UploadImageViewModel(
            connectivityStatusProvider, uploadImageUseCase, schedulersFacade
        )
    }

    @Test
    fun `test uploadImage if no network connection`() {
        ArrangeBuilder()
            .withConnectivityStatus(isConnected = false)

        underTest.uploadImage(uri)

        verifyZeroInteractions(uploadImageUseCase)
        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.Error::class.java)
        )

        assertThat(
            (underTest.uiEventLiveData.value as UiEvent.Error).throwable,
            instanceOf(NoNetworkException::class.java)
        )
    }


    @Test
    fun `test uploadImage if success event`() {
        //given
        val argumentCaptor = argumentCaptor<UiEvent>()

        ArrangeBuilder()
            .withConnectivityStatus(isConnected = true)
            .completeUploadImageUseCase()

        underTest.uiEventLiveData.observeForever(observer)

        //when
        underTest.uploadImage(uri)

        //then
        verify(uploadImageUseCase).execute(uri)
        argumentCaptor.run {
            verify(observer, times(2)).onChanged(capture())

            assertThat(
                firstValue,
                instanceOf(UiEvent.Loading::class.java)
            )

            assertThat(
                lastValue,
                instanceOf(UiEvent.ImageUploaded::class.java)
            )
        }

        underTest.uiEventLiveData.removeObserver(observer)
    }

    @Test
    fun `test uploadImage if exception was thrown`() {
        //given
        val exception: Throwable = RuntimeException()
        val argumentCaptor = argumentCaptor<UiEvent>()

        ArrangeBuilder()
            .withConnectivityStatus(isConnected = true)
            .withExpectedException(exception)
        underTest.uiEventLiveData.observeForever(observer)

        //when
        underTest.uploadImage(uri)

        //then
        verify(uploadImageUseCase).execute(uri)
        argumentCaptor.run {
            verify(observer, times(2)).onChanged(capture())

            assertThat(
                firstValue,
                instanceOf(UiEvent.Loading::class.java)
            )

            assertThat(
                lastValue,
                instanceOf(UiEvent.Error::class.java)
            )

            assertThat(
                (lastValue as UiEvent.Error).throwable,
                equalTo(exception)
            )
        }

        underTest.uiEventLiveData.removeObserver(observer)
    }

    @Test
    fun `test onUploadImageClick`() {
        underTest.onUploadImageClick()

        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.UploadImageClicked::class.java)
        )
    }

    @Test
    fun `test onProcessImageClick`() {
        underTest.onProcessImageClick()

        assertThat(
            underTest.uiEventLiveData.value,
            instanceOf(UiEvent.ProcessImage::class.java)
        )
    }

    private inner class ArrangeBuilder {
        fun completeUploadImageUseCase(): ArrangeBuilder {
            whenever(uploadImageUseCase.execute(uri)).thenReturn(Completable.complete())
            return this
        }

        fun withExpectedException(throwable: Throwable): ArrangeBuilder {
            whenever(uploadImageUseCase.execute(uri)).thenReturn(
                Completable.error(throwable)
            )
            return this
        }

        fun withConnectivityStatus(isConnected: Boolean): ArrangeBuilder {
            whenever(connectivityStatusProvider.isConnected()).thenReturn(isConnected)
            return this
        }
    }
}