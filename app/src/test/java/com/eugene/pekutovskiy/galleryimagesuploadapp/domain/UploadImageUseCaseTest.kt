package com.eugene.pekutovskiy.galleryimagesuploadapp.domain

import android.net.Uri
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo.ImagesRepository
import com.eugene.pekutovskiy.testutil.TestDataUtil
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Completable
import org.junit.Test

class UploadImageUseCaseTest {

    private val repo: ImagesRepository = mock()
    private lateinit var underTest: UploadImageUseCase

    @Test
    fun `test execute of UploadImageUseCase`() {
        underTest = UploadImageUseCase(repo)

        val uri: Uri = TestDataUtil.GALLERY_DATA_URI[0]
        ArrangeBuilder().withUri(uri)

        underTest.execute(uri)
            .test()
            .assertComplete()
            .assertNoErrors()
            .dispose()

        verify(repo).uploadImage(uri)
    }

    inner class ArrangeBuilder {
        fun withUri(uri: Uri): ArrangeBuilder {
            whenever(repo.uploadImage(uri)).thenReturn(Completable.complete())
            return this
        }
    }
}