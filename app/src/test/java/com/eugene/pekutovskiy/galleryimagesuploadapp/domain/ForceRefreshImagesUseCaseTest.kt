package com.eugene.pekutovskiy.galleryimagesuploadapp.domain

import android.net.Uri
import com.eugene.pekutovskiy.galleryimagesuploadapp.data.repo.ImagesRepository
import com.eugene.pekutovskiy.testutil.TestDataUtil
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import org.junit.Test

class ForceRefreshImagesUseCaseTest {

    private val repo: ImagesRepository = mock()
    private val items: List<Uri> = TestDataUtil.GALLERY_DATA_URI
    private lateinit var underTest: ForceRefreshImagesUseCase

    @Test
    fun `test execute call`() {
        underTest = ForceRefreshImagesUseCase(repo)

        ArrangeBuilder()
            .withItems(items)

        underTest.execute()
            .test()
            .assertValue(items)
            .dispose()

        verify(repo, times(1)).forceRefreshImages()
    }

    private inner class ArrangeBuilder {
        fun withItems(items: List<Uri>): ArrangeBuilder {
            whenever(repo.forceRefreshImages()).thenReturn(Single.just(items))
            return this
        }
    }
}