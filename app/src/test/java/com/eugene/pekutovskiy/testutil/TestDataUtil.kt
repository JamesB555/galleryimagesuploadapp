package com.eugene.pekutovskiy.testutil

import android.net.Uri
import com.nhaarman.mockitokotlin2.mock

internal object TestDataUtil {
    val GALLERY_DATA_STR = listOf(
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2F2-287.jpg?alt=media&token=b415a60f-0010-437a-9b9b-0bfbff9358c8",
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2F55fe474c-efa5-46f3-a2a1-7f2cf0c661ec.png?alt=media&token=1da71341-e1bb-4de1-a9af-4268bed1091a"
    )
    val GALLERY_DATA_URI = listOf<Uri>(
        mock(),
        mock()
    )
}