package com.eugene.pekutovskiy.testutil

import com.eugene.pekutovskiy.galleryimagesuploadapp.util.rx.SchedulersFacade
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

class TestSchedulersFacade : SchedulersFacade() {
    override fun io(): Scheduler = Schedulers.trampoline()

    override fun computation(): Scheduler = Schedulers.trampoline()

    override fun main(): Scheduler = Schedulers.trampoline()
}