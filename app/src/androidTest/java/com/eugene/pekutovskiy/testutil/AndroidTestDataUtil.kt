package com.eugene.pekutovskiy.testutil

object AndroidTestDataUtil {

    val GALLERY_DATA_STR = listOf(
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2F18a11c60-41fa-4a17-af55-ac031db654a8.jpg?alt=media&token=6895534c-fd24-4d08-8dd6-0e9038553f5a",
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2F2-287.jpg?alt=media&token=b415a60f-0010-437a-9b9b-0bfbff9358c8",
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2F55fe474c-efa5-46f3-a2a1-7f2cf0c661ec.png?alt=media&token=1da71341-e1bb-4de1-a9af-4268bed1091a",
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2FRed-Panda-Parker-001-Birmingham-Zoo-2-27-18-1024x801.jpg?alt=media&token=19284533-7adf-4e47-bf20-fe7ac64ea8e3",
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2Fc4dc9565-68ac-4c67-be84-c5de2faad9eb.jpg?alt=media&token=e66ed4b1-f181-490b-a3fb-2cf340d4b49f",
        "https://firebasestorage.googleapis.com/v0/b/galleryimagesuploadapp.appspot.com/o/images%2Fhamster-1055x675.jpeg?alt=media&token=b709138a-e3af-4282-a2ad-4107ec7542fc"
    )
}