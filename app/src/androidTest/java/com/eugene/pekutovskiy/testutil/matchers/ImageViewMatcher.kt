package com.eugene.pekutovskiy.testutil.matchers

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

object ImageViewMatcher {

    fun hasDrawable(): TypeSafeMatcher<View> {
        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("ImageView should have a drawable")
            }

            override fun matchesSafely(item: View): Boolean {
                val imageView = (item as AppCompatImageView)
                return imageView.drawable != null
            }
        }
    }
}