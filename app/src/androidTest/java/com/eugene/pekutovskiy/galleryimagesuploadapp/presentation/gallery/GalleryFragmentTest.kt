package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery


import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeDown
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.eugene.pekutovskiy.galleryimagesuploadapp.R
import com.eugene.pekutovskiy.galleryimagesuploadapp.SparkNetworksApp
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.MainActivity
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.NetworkInfo
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.adapter.GalleryListAdapter
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.adapter.GalleryListDiffCallback
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser.ChooseImageDialog
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser.ChooseImageDialog.PickupOption.FROM_CAMERA
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser.ChooseImageDialog.PickupOption.FROM_GALLERY
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.gallery.imagechooser.ImageChooserDelegate
import com.eugene.pekutovskiy.testutil.AndroidTestDataUtil
import com.eugene.pekutovskiy.testutil.createFakeFragmentInjector
import com.eugene.pekutovskiy.testutil.createFakeViewModelFactory
import com.eugene.pekutovskiy.testutil.databinding.DataBindingIdlingResourceRule
import com.eugene.pekutovskiy.testutil.matchers.RecyclerViewMatcher
import dagger.android.DispatchingAndroidInjector
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.atLeastOnce
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class GalleryFragmentTest {

    @Mock
    lateinit var viewModel: GalleryViewModel
    @Mock
    lateinit var imageChooser: ImageChooserDelegate
    var adapter: GalleryListAdapter = GalleryListAdapter(GalleryListDiffCallback())

    private val uiEventLiveData = MutableLiveData<GalleryViewModel.UiEvent>()
    private val connectivityStatusLiveData = MutableLiveData<NetworkInfo>()
    private val appContext = InstrumentationRegistry.getInstrumentation()
        .targetContext.applicationContext as SparkNetworksApp
    var galleryFragment: GalleryFragment? = null

    lateinit var originalDispatchingActivityInjector: DispatchingAndroidInjector<Any>

    @Rule
    @JvmField
    val activityRule =
        object : ActivityTestRule<MainActivity>(MainActivity::class.java, false, false) {
            override fun beforeActivityLaunched() {
                super.beforeActivityLaunched()
                originalDispatchingActivityInjector = appContext.dispatchingAndroidInjector
                appContext.dispatchingAndroidInjector =
                    createFakeFragmentInjector<GalleryFragment> {
                        this.viewModelFactory = createFakeViewModelFactory(viewModel)
                        this.galleryListAdapter = adapter
                        this.imageChooserDelegate = imageChooser
                        this.galleryViewModel = viewModel
                        galleryFragment = this
                    }
            }

            override fun afterActivityFinished() {
                appContext.dispatchingAndroidInjector = originalDispatchingActivityInjector
            }
        }

    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    @Before
    fun setUp() {
        val arrangeBuilder = ArrangeBuilder()
        arrangeBuilder.substituteLiveData()
        activityRule.launchActivity(null)
        arrangeBuilder.postGalleryUpdateEvent()
    }

    @Test
    fun testGalleryList() {
        val itemsCount = AndroidTestDataUtil.GALLERY_DATA_STR.size

        //verify list is displayed and populated correctly
        onView(withId(R.id.recyclerView)).check(matches(isDisplayed()))
        onView(withId(R.id.recyclerView)).check(matches(RecyclerViewMatcher.hasItemsCount(itemsCount)))
        // verify toast is shown on item click
        onView(RecyclerViewMatcher.atPositionOnView(R.id.recyclerView, 0)).perform(click())
        onView(withText(R.string.item_clicked_stub))
            .inRoot(withDecorView(not(`is`(activityRule.activity.window.decorView))))
            .check(matches(isDisplayed()))

        // assert toolbar has correct title
        assertThat(
            activityRule.activity.title.toString(),
            `is`(equalTo(appContext.getString(R.string.gallery)))
        )
    }

    @Test
    fun testGalleryListOnNoNetwork() {
        connectivityStatusLiveData.postValue(NetworkInfo.NO_NETWORK)

        //verify "no network" message is shown
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.no_network_error_msg)))
    }

    @Test
    fun testSwipeRefresh() {
        onView(withId(R.id.swipeRefreshLayout)).perform(swipeDown())
        verify(viewModel).onSwipeRefresh()
    }

    @Test
    fun testChooseImageDialog_when_choosing_from_gallery() {

        //verify fab is displayed, perform click
        onView(withId(R.id.fab)).check(matches(isDisplayed()))
        onView(withId(R.id.fab)).perform(click())
        //verify onFabClicked() is called
        verify(viewModel).onFabClicked()

        ArrangeBuilder().postShowChooseImageDialogEvent()

        //verify that dialog is displayed
        onView(withId(R.id.choosePictureDialog))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))

        // verify corresponding imageChooser's method has been called
        onView(withId(R.id.pickFromGallery)).perform(click())
        ArrangeBuilder().withPickupOptionCommand(FROM_GALLERY)
        verify(imageChooser, atLeastOnce()).pickFromGallery(galleryFragment!!)
    }

    @Test
    fun testChooseImageDialog_when_choosing_from_camera() {

        //verify fab is displayedv
        onView(withId(R.id.fab)).check(matches(isDisplayed()))
        onView(withId(R.id.fab)).perform(click())
        //verify onFabClicked() is called
        verify(viewModel).onFabClicked()

        ArrangeBuilder().postShowChooseImageDialogEvent()

        //verify that dialog is displayed
        onView(withId(R.id.choosePictureDialog))
            .inRoot(isDialog())
            .check(matches(isDisplayed()))

        // verify corresponding imageChooser's method has been called
        onView(withId(R.id.takeFromCamera)).perform(click())
        ArrangeBuilder().withPickupOptionCommand(FROM_CAMERA)
        verify(imageChooser, atLeastOnce()).takeFromCamera(galleryFragment!!)
    }

    private inner class ArrangeBuilder {

        fun substituteLiveData(): ArrangeBuilder {
            Mockito.`when`(viewModel.uiEventLiveData).thenReturn(uiEventLiveData)
            Mockito.`when`(viewModel.connectivityStatusLiveData)
                .thenReturn(connectivityStatusLiveData)
            return this
        }

        fun postGalleryUpdateEvent(): ArrangeBuilder {
            val event = GalleryViewModel.UiEvent.GalleryUpdate(
                AndroidTestDataUtil.GALLERY_DATA_STR
            )

            uiEventLiveData.postValue(event)
            return this
        }

        fun postShowChooseImageDialogEvent(): ArrangeBuilder {
            uiEventLiveData.postValue(
                GalleryViewModel.UiEvent.ShowChooseImageDialog
            )
            return this
        }

        fun withPickupOptionCommand(command: ChooseImageDialog.PickupOption) {
            val callback: ChooseImageDialog.ChoosePickOptionCallback =
                requireNotNull(galleryFragment)
            callback.onOptionHasChosen(command)
        }
    }

}