package com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.uploadImage

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.eugene.pekutovskiy.galleryimagesuploadapp.R
import com.eugene.pekutovskiy.galleryimagesuploadapp.SparkNetworksApp
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.MainActivity
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.NetworkInfo
import com.eugene.pekutovskiy.galleryimagesuploadapp.presentation.common.UPLOAD_IMAGE_FRAGMENT
import com.eugene.pekutovskiy.testutil.AndroidTestDataUtil
import com.eugene.pekutovskiy.testutil.createFakeFragmentInjector
import com.eugene.pekutovskiy.testutil.createFakeViewModelFactory
import com.eugene.pekutovskiy.testutil.databinding.DataBindingIdlingResourceRule
import com.eugene.pekutovskiy.testutil.matchers.ImageViewMatcher
import dagger.android.DispatchingAndroidInjector
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UploadImageFragmentTest {

    @Mock
    lateinit var viewModel: UploadImageViewModel

    private val connectivityStatusLiveData = MutableLiveData<NetworkInfo>()
    val uiEventLiveData = MutableLiveData<UploadImageViewModel.UiEvent>()
    private val uri = Uri.parse(AndroidTestDataUtil.GALLERY_DATA_STR[0])
    private val appContext = InstrumentationRegistry.getInstrumentation()
        .targetContext.applicationContext as SparkNetworksApp

    lateinit var originalDispatchingActivityInjector: DispatchingAndroidInjector<Any>

    @Rule
    @JvmField
    val activityRule =
        object : ActivityTestRule<MainActivity>(MainActivity::class.java, false, false) {
            override fun beforeActivityLaunched() {
                super.beforeActivityLaunched()
                originalDispatchingActivityInjector = appContext.dispatchingAndroidInjector
                appContext.dispatchingAndroidInjector =
                    createFakeFragmentInjector<UploadImageFragment> {
                        this.viewModelFactory = createFakeViewModelFactory(viewModel)
                        this.arguments = Bundle().apply {
                            putParcelable(UploadImageFragment.PIC_URI, uri)
                        }
                    }
            }

            override fun afterActivityFinished() {
                appContext.dispatchingAndroidInjector = originalDispatchingActivityInjector
            }
        }

    @Rule
    @JvmField
    val dataBindingIdlingResourceRule = DataBindingIdlingResourceRule(activityRule)

    @Before
    fun setUp() {
        val arrangeBuilder = ArrangeBuilder()
        arrangeBuilder.substituteLiveData()
        activityRule.launchActivity(buildLaunchIntent())
    }

    private fun buildLaunchIntent() =
        Intent().putExtra(MainActivity.EXTRA_FRAGMENT_ID, UPLOAD_IMAGE_FRAGMENT)

    @Test
    fun testFragmentDisplaysProperUi() {
        // assert toolbar has correct title
        assertThat(
            activityRule.activity.title.toString(),
            Matchers.`is`(Matchers.equalTo(appContext.getString(R.string.new_post)))
        )

        //verify both buttons are visible but progressBar is not visible
        onView(withId(R.id.processImageButton)).check(matches(isDisplayed()))
        onView(withId(R.id.uploadButton)).check(matches(isDisplayed()))
        onView(withId(R.id.progress)).check(matches(withEffectiveVisibility(Visibility.GONE)))

        // verify that ImageView displays picture image
        onView(withId(R.id.image)).check(matches(isDisplayed()))
        onView(withId(R.id.image)).check(matches(ImageViewMatcher.hasDrawable()))
    }

    @Test
    fun testUploadImageClick() {

        //check that upload button is visible but progress bur is not
        onView(withId(R.id.uploadButton)).check(matches(isDisplayed()))
        onView(withId(R.id.progress)).check(matches(not(isDisplayed())))
        //perform click on "upload button"
        onView(withId(R.id.uploadButton)).perform(click())

        //verify onUploadImageClick() is called
        Mockito.verify(viewModel).onUploadImageClick()
        uiEventLiveData.postValue(UploadImageViewModel.UiEvent.UploadImageClicked)
        //verify button is not clickable
        onView(withId(R.id.uploadButton)).check(matches(not(isEnabled())))

        //verify progressbar is shown
        uiEventLiveData.postValue(UploadImageViewModel.UiEvent.Loading)
        onView(withId(R.id.progress)).check(matches(isDisplayed()))

        //  image has been uploaded successfully
        uiEventLiveData.postValue(UploadImageViewModel.UiEvent.ImageUploaded)

        //verify progressbar is no longer displayed
        onView(withId(R.id.progress)).check(matches(not(isDisplayed())))
        //verify snackbar with success message is shown
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.image_has_been_uploaded)))
    }

    @Test
    fun testGalleryListOnNoNetwork() {
        connectivityStatusLiveData.postValue(NetworkInfo.NO_NETWORK)

        //verify no network message is shown
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText(R.string.no_network_error_msg)))
    }

    /// .... other tests


    private inner class ArrangeBuilder {
        fun substituteLiveData(): ArrangeBuilder {
            Mockito.`when`(viewModel.uiEventLiveData).thenReturn(uiEventLiveData)
            Mockito.`when`(viewModel.connectivityStatusLiveData)
                .thenReturn(connectivityStatusLiveData)
            return this
        }
    }
}